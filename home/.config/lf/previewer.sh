#!/bin/sh
# Custom LF file preview script

case "$1" in
    *.tar*) tar tf "$1";;
    *.zip) unzip -l "$1";;
    *.rar) unrar l "$1";;
    *.7z) 7z l "$1";;
    *.pdf) pdftotext "$1" -;;
    *) highlight $1 -O ansi || cat "$1";;
    # *) pygmentize $1 || cat "$1";;
esac
